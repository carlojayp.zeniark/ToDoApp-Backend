require('dotenv').config();

const mysql = require('mysql2'); 

const options = {
    host: process.env.MYSQL_DB_HOST, 
    user: process.env.MYSQL_DB_USER,
    password: process.env.MYSQL_DB_PASS,
    database: process.env.MYSQL_DB_NAME,
    port: process.env.MYSQL_DB_PORT
};
const pool = mysql.createPool(options);

pool.getConnection((err) => {
    if (err) {
    	console.error({'MySQL Options': options});
        throw err
    };

    console.info('Connected from MySQL database...');
});

exports.query = (query, values) => {
    return pool.promise().query(query, values)
        .then(([rows, fields]) => {
            return rows;
        })
        .catch((err) => {
            console.error('MySQL: Query Interrupted');
            console.error({
                'Query': query,
                'Error': err
            });
            throw err;
        });
};