const jwt = require('jsonwebtoken');

exports.validateToken = (req, res, next) => {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(' ')[1];
        req.token = bearerToken;
        jwt.verify(req.token, process.env.JWT_SECRET, (err, authData) => {
            if (err) {
                res.status(403).send(
                    {
                        success: 'false',
                        message: 'Invalid token'
                    }
                );
            } else {
                next();
            }
        });
    } else {
        res.status(403).send(
            {
                success: 'false',
                message: 'Invalid token'
            }
        );
    }
}