require('dotenv').config();

const auth = require('../middlewares/auth.middleware');
const Users = require('../controllers/users.controller.js');

module.exports = (app) => {

    // ADMIN ROUTES
    {
        app.get(process.env.BASE_URL + '/admin/users', auth.validateToken, Users.getAll);

        app.get(process.env.BASE_URL + '/admin/users/:username', auth.validateToken, Users.getOneByUsername);
    
        app.post(process.env.BASE_URL + '/admin/users', Users.create);
    
        app.put(process.env.BASE_URL + '/admin/users/:username', auth.validateToken, Users.update);
    
        app.delete(process.env.BASE_URL + '/admin/users/:username', auth.validateToken, Users.delete);
    }

    {
        app.post(process.env.BASE_URL + '/login', Users.login)
    }

}