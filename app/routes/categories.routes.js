require('dotenv').config();

const auth = require('../middlewares/auth.middleware');
const Categories = require('../controllers/categories.controller.js');

module.exports = (app) => {

    // ADMIN ROUTES
    {
        app.get(process.env.BASE_URL + '/admin/categories', auth.validateToken, Categories.getAll);

        app.get(process.env.BASE_URL + '/admin/categories/:id', auth.validateToken, Categories.getOneByID);

        app.post(process.env.BASE_URL + '/admin/categories', auth.validateToken, Categories.create);
        
        app.put(process.env.BASE_URL + '/admin/categories/:id', auth.validateToken, Categories.update);
    
        app.delete(process.env.BASE_URL + '/admin/categories/:id', auth.validateToken, Categories.delete);
    }

}