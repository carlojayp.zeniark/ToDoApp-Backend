require('dotenv').config();

const auth = require('../middlewares/auth.middleware');
const Tasks = require('../controllers/tasks.controller.js');

module.exports = (app) => {

    // ADMIN ROUTES
    {
        // app.get(process.env.BASE_URL + '/admin/tasks', auth.validateToken, Tasks.findAll);

        app.get(process.env.BASE_URL + '/admin/tasks/:id', auth.validateToken, Tasks.findOne);
    
        app.post(process.env.BASE_URL + '/admin/tasks', auth.validateToken, Tasks.create);
    
        // app.put(process.env.BASE_URL + 'admin/tasks/:id', auth.validateToken, Tasks.update);
    
        // app.delete(process.env.BASE_URL + 'admin/tasks/:id', auth.validateToken, Tasks.delete);
    }

    // USER ROUTES
    {
        app.get(process.env.BASE_URL + '/users/:user_id/tasks/', auth.validateToken, Tasks.findAllTaskByUser);

        app.put(process.env.BASE_URL + '/users/tasks/:id/change_status', auth.validateToken, Tasks.changeStatus);
        
        // app.get(process.env.BASE_URL + '/users/:user_id/tasks/:task_id', auth.validateToken, Tasks.findTaskByUser);
    }

}