class Task {
    constructor(user_id, category_id, title, description) {
        if (!user_id) {
            throw new Error('user_id is required')
        }
        if (!category_id) {
            throw new Error('category_id is required')
        }
        if (!title) {
            throw new Error('title is required')
        }
        if (!description) {
            throw new Error('description is required')
        }
        this.user_id = user_id;
        this.category_id = category_id;
        this.title = title;
        this.description = description;
        this.is_done = 0;
        this.created_at = new Date();
        this.updated_at = new Date();
    }

    setValues() {
        return {
            user_id: this.user_id,
            category_id: this.category_id,
            title: this.title,
            description: this.description,
            is_done: this.is_done,
            created_at: this.created_at,
            updated_at: this.updated_at
        }
    }
}

module.exports = Task;