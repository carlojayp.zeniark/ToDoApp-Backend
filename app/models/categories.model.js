class Category {

    constructor(name) {
        if (!name) {
            throw new Error('name is required')
        }
        this.name = name;
        this.created_at = new Date();
        this.updated_at = new Date();
    }

    setValues() {
        return {
            name: this.name,
            created_at: this.created_at,
            updated_at: this.updated_at
        }
    }

}

module.exports = Category;