class User {

    constructor(first_name, last_name, username, password, salt) {
        if (!first_name){
            throw new Error('first_name is required')
        }
        if (!last_name){
            throw new Error('last_name is required')
        }
        if (!username){
            throw new Error('username is required')
        }
        if (!password){
            throw new Error('password is required')
        }
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.created_at = new Date();
        this.updated_at = new Date();        
    }

    setValues() {
        return {
            first_name: this.first_name,
            last_name: this.last_name,
            username: this.username,
            password: this.password,
            salt: this.salt,
            created_at: this.created_at,
            updated_at: this.updated_at
        }
    }
        
}

module.exports = User;
