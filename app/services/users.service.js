const connection = require('../../config/mysql.js');
const User = require('../models/user.model.js');
const bcrypt = require('bcrypt');

exports.create = async (params) => {

    const { first_name, last_name, username, password } = params;
    const salt = await bcrypt.genSalt(10);

    const hashedPassword = await hashPassword(password, salt);

    const values = new User(first_name, last_name, username, hashedPassword, salt).setValues();
    const query = 'INSERT INTO `users` SET ?';

    const entry = await connection.query(query, values);
    return entry;
}

exports.login = async (params) => {

    const { username, password } = params;
    const query = 'SELECT `password`, `salt` FROM `users` WHERE `username` = ?';
    const user = await connection.query(query, [username]);
    const hashedPassword = user[0].password;
    const salt = user[0].salt;
    const isMatch = await comparePassword(password, hashedPassword);
    return isMatch;
    
}

exports.getAll = async () => {
    const query = 'SELECT `first_name`, `last_name`, `username` FROM `users`';
    const users = await connection.query(query);
    return {users};
}

exports.getOneByUsername = async (username) => {
    const query = 'SELECT `first_name`, `last_name`, `username` FROM `users` WHERE `username` = ?';
    const user = await connection.query(query, [username]);
    return user[0];
}

exports.getIDByUsername = async (username) => {
    const query = 'SELECT `id` FROM `users` WHERE `username` = ?';
    const user = await connection.query(query, [username]);
    return user[0].id;
}

exports.update = async (username, params) => {
    let entry;

    if (params.first_name) {
        const query = 'UPDATE `users` SET `first_name` = ? WHERE `username` = ?';
        const entry = await connection.query(query, [params.first_name, username]);
    }
    if (params.last_name) {
        const query = 'UPDATE `users` SET `last_name` = ? WHERE `username` = ?';
        const entry = await connection.query(query, [params.last_name, username]);
    }

    return entry;
}

exports.delete = async (username) => {
    const query = 'DELETE FROM `users` WHERE `username` = ?';
    const entry = await connection.query(query, [username]);
    return entry;
}

const hashPassword = async (password, salt) => {
    const hashedPassword = await bcrypt.hash(password, salt);
    return hashedPassword;
}

const comparePassword = async (password, hashedPassword) => {
    const isMatch = await bcrypt.compare(password, hashedPassword);
    return isMatch;
}