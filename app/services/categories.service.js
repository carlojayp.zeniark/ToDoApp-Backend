const connection = require('../../config/mysql.js');
const Category = require('../models/categories.model.js');

exports.create = async (params) => {
    
    const { name } = params;

    const values = new Category(name).setValues();
    const query = 'INSERT INTO `categories` SET ?';

    const entry = await connection.query(query, values);
    return entry;

}

exports.getAll = async () => {

    const query = 'SELECT `id`, `name`, `created_at`, `updated_at` FROM `categories`';
    const categories = await connection.query(query);
    return {categories};

}

exports.getOneByID = async (id) => {

    const query = 'SELECT `id`, `name`, `created_at`, `updated_at` FROM `categories` WHERE `id` = ?';
    const category = await connection.query(query, [id]);
    return category[0];

}

exports.update = async (id, params) => {
    let entry;

    if (params.name) {
        const query = 'UPDATE `categories` SET `name` = ? WHERE `id` = ?';
        const entry = await connection.query(query, [params.name, id]);
    }

    return entry;
}

exports.delete = async (id) => {
    
    const query = 'DELETE FROM `categories` WHERE `id` = ?';
    const entry = await connection.query(query, [id]);
    return entry;
    
}