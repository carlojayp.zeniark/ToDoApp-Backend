const connection = require('../../config/mysql.js');
const Task = require('../models/task.model.js');

exports.create = async (params) => {
        
    const { user_id, category_id, title, description } = params;

    const values = new Task(user_id, category_id, title, description).setValues();
    const query = 'INSERT INTO `tasks` SET ?';

    const entry = await connection.query(query, values);
    return entry;
    
}

exports.findAllTaskByUser = async (user_id) => {
    const query = 'SELECT * FROM `tasks` JOIN `categories` on tasks.category_id = categories.id WHERE `user_id` = ?';
    const tasks = await connection.query(query, user_id);
    return tasks;
}

exports.changeStatus = async (id) => {
    const query = 'UPDATE `tasks` SET `is_done` = 1 WHERE `id` = ?';
    const task = await connection.query(query, id);
    return task;
}

exports.findOne = async (id) => {
    const query = 'SELECT * FROM `tasks` JOIN `categories` ON tasks.category_id = categories.id JOIN `users` ON tasks.user_id = users.id WHERE tasks.id = ?';
    const task = await connection.query(query, [id]);
    return {
        category: task[0].name,
        user: task[0].first_name + ' ' + task[0].last_name,
        title: task[0].title,
        description: task[0].description,
        is_done: task[0].is_done,
        created_at: task[0].created_at,
        updated_at: task[0].updated_at,
    };
}