const categoriesService = require('../services/categories.service');

exports.create = async (req, res) => {
    try {

        const params = req.body;

        const category = await categoriesService.create(params);
        if (!category) {
            res.status(400).send(
                {
                    success: 'false',
                    message: 'Failed to create Category!'
                }
            );
            return false;
        }

        res.status(200).send(
            {
                success: 'true',
                message: 'Category created successfully',
            }
        );

    }
    catch (error) {
        res.status(500).send(
            {
                success: 'false',
                message: error.message
            }
        );
    }
}

exports.getAll = async (req, res) => {
    try {
        const categories = await categoriesService.getAll();

        res.status(200).send({
            success: true,
            data: categories
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.getOneByID = async (req, res) => {
    try {
        const id = req.params.id;

        const category = await categoriesService.getOneByID(id);

        res.status(200).send({
            success: true,
            data: category
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.update = async (req, res) => {
    try {
        const id = req.params.id;
        const params = req.body;

        const category = await categoriesService.update(id, params);

        res.status(200).send({
            success: true,
            message: 'Category updated successfully'
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.delete = async (req, res) => {
    try {
        const id = req.params.id;

        const category = await categoriesService.delete(id);

        res.status(200).send({
            success: true,
            message: 'Category deleted successfully'
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}