require('dotenv').config();
const userService = require('../services/users.service');
const jwt = require('jsonwebtoken');

exports.login = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await userService.getOneByUsername(username);
        if (!user) {
            return res.status(401).json({ message: 'Authentication failed' });
        }
        const isMatch = await userService.login({username, password});
        if (!isMatch) {
            return res.status(401).json({ message: 'Authentication failed' });
        }

        const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, { expiresIn: '1h' });
        res.status(200).json({ token });
    }
    catch (error) {
        res.status(500).send(
            {
                success: 'false',
                message: error.message
            }
        );
    }
}

exports.create = async (req, res) => {
    try {
        const params  = req.body;

        const user = await userService.create(params);
        if (!user) {
            res.status(400).send(
                {
                    success: 'false',
                    message: 'Failed to create User!'
                }
            );
            return false;
        }

        res.status(200).send(
            {
                success: 'true',
                message: 'User created successfully',
            }
        );
    }
    catch (error) {
        res.status(500).send(
            {
                success: 'false',
                message: error.message
            }
        );
    }
}

exports.getAll = async (req, res) => {
    try {
        console.log(process.env.TEST_ENV)

        const users = await userService.getAll();  

        res.status(200).send({
            success: true,
            data: users
        })

    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.getOneByUsername = async (req, res) => {
    try {
        const username = req.params.username;

        const user = await userService.getOneByUsername(username);

        res.status(200).send({
            success: true,
            data: user
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.update = async (req, res) => {
    try {
        const username = req.params.username;
        const params = req.body;

        const user = await userService.update(username, params);

        res.status(200).send({
            success: true,
            message: 'User updated successfully'
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.delete = async (req, res) => {
    try {
        const username = req.params.username;

        const user = await userService.delete(username);

        res.status(200).send({
            success: true,
            message: 'User deleted successfully'
        })
    } 
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })    
    }
}

exports.addDueDate = async (req, res) => {
    try {
        // Added Due Date to Task
    } catch (error) {

    }
}