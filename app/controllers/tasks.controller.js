const todoService = require('../services/tasks.service');

exports.findAll = async (req, res) => {
    res.send({
        success: 'true',
        message: 'todos retrieved successfully',
    })
}

exports.create = async (req, res) => {

    const params  = req.body;

    const entry = await todoService.create(params);

    res.status(200).send(
        {
            success: 'true',
            message: 'Task created successfully'
        }
    );
}

exports.findAllTaskByUser = async (req, res) => {
    try {
        const user_id = req.params.user_id;

        const tasks = await todoService.findAllTaskByUser(user_id);

        res.status(200).send({
            success: true,
            data: tasks
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.changeStatus = async (req, res) => {
    try {
        const id = req.params.id;

        const task = await todoService.changeStatus(id);

        res.status(200).send({
            success: true,
            data: task
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}

exports.findOne = async (req, res) => {
    try {
        const id = req.params.id;

        const task = await todoService.findOne(id);

        res.status(200).send({
            success: true,
            data: task
        })
    }
    catch (error) {
        res.status(500).send({
            success: false,
            message: error.message
        })
    }
}


