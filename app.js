const PORT = 3003;

// Express Initialization and Other Configurations
const express = require('express');
const app = express();
const cors = require('cors');


// Middlewares
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).send(
        {
            success: 'true',
            message: 'Welcome to the Node.js, Express, and MySQL ToDo API'
        }   
    );
});

// Route Initialization
require('./app/routes/tasks.routes.js')(app);
require('./app/routes/users.routes.js')(app);
require('./app/routes/categories.routes.js')(app);

// Initialize the server and run it on the specified port
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
